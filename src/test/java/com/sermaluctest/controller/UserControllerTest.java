package com.sermaluctest.controller;

import com.sermaluctest.exceptionhandler.custom.UserMissingDataException;
import com.sermaluctest.model.Phone;
import com.sermaluctest.model.User;
import com.sermaluctest.service.UserService;
import com.sermaluctest.utils.JSONUtils;
import com.sermaluctest.utils.Utils;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class UserControllerTest {

    private final UserService userService = mock(UserService.class);
    private final Utils utils = mock(Utils.class);
    UserController userController = new UserController(userService);

    @Test
    public void shouldBeASuccessfulCall() {
        ResponseEntity<Object> successResponse = new ResponseEntity<>(Utils.toJSONFromObject(null), HttpStatus.CREATED);
        User user = JSONUtils.readJson("/user-register-payload.json", User.class);

        when(userService.saveUser(any())).thenReturn(isA(User.class));
        ResponseEntity<String> controllerResponse = userController.addUser(user);

        verify(userService, times(1)).saveUser(isA(User.class));
        assertEquals(successResponse, controllerResponse);
    }
}
