package com.sermaluctest.constant;

public class SermalucConstants {

    public static final String REGEX_EMAIL_VALIDATION = "^[a-zA-Z0-9_.-]+@[a-zA-Z0-9.-]+.[a-zA-Z]+$";

}
