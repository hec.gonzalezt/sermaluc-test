package com.sermaluctest.repository;

import com.sermaluctest.model.Phone;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface PhoneRepository extends CrudRepository<Phone, UUID> {
}
