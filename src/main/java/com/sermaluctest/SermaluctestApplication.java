package com.sermaluctest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SermaluctestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SermaluctestApplication.class, args);
	}

}
